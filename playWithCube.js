// WebGL - Perspective Correct Textured Cube (the default)
// from https://webglfundamentals.org/webgl/webgl-perspective-correct-cube.html

  "use strict";

function main() {
  // Get A WebGL context
  /** @type {HTMLCanvasElement} */
  var canvas = document.getElementById("canvas");
  var gl = canvas.getContext("webgl");
  if (!gl) {
    return;
  }
  var depthTextureExt = gl.getExtension("WEBGL_depth_texture");
  console.log(depthTextureExt);
  if(!depthTextureExt) { console.log("poop"); return; }

  // setup GLSL programs
  var program = webglUtils.createProgramFromScripts(gl, ["3d-vertex-shader", "3d-fragment-shader"]);
  var program_post = webglUtils.createProgramFromScripts(gl, ["3d-vertex-shader", "3d-fragment-shader-post"]);

  // look up where the vertex data needs to go.
  var positionLocation = gl.getAttribLocation(program, "a_position");
  var texcoordLocation = gl.getAttribLocation(program, "a_texcoord");

  // lookup uniforms
  var matrixLocation = gl.getUniformLocation(program, "u_matrix");
  var textureLocation = gl.getUniformLocation(program, "u_texture");
  var depthTextureLocation = gl.getUniformLocation(program, "d_texture");
  var depthBlurringLocation = gl.getUniformLocation(program, "depth_blurring");
  var focalDistLocation = gl.getUniformLocation(program, "focal_dist");

  var matrixLocation_post = gl.getUniformLocation(program_post, "u_matrix");
  var positionLocation_post = gl.getAttribLocation(program_post, "a_position");
  var texcoordLocation_post = gl.getAttribLocation(program_post, "a_texcoord");
  var tex_curLocation = gl.getUniformLocation(program_post, "tex_cur");
  var tex_prev_1Location = gl.getUniformLocation(program_post, "tex_prev_1");
  var tex_prev_2Location = gl.getUniformLocation(program_post, "tex_prev_2");

  // START MAKE TEXTURE

  // create to render to
  const targetTextureWidth = 512;
  const targetTextureHeight = 512;
  const targetTexture = gl.createTexture();
  gl.activeTexture(gl.TEXTURE0)
  gl.bindTexture(gl.TEXTURE_2D, targetTexture);

  {
    // define size and format of level 0
    const level2 = 0;
    const internalFormat2 = gl.RGBA;
    const border2 = 0;
    const format2 = gl.RGBA;
    const type2 = gl.UNSIGNED_BYTE;
    const data2 = null;
    gl.texImage2D(gl.TEXTURE_2D, level2, internalFormat2,
                  targetTextureWidth, targetTextureHeight, border2,
                  format2, type2, data2);

    // set the filtering so we don't need mips
    gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, gl.LINEAR);
    gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_S, gl.CLAMP_TO_EDGE);
    gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_T, gl.CLAMP_TO_EDGE);
  }

  // create to render to
  const targetTexture2 = gl.createTexture();
  gl.activeTexture(gl.TEXTURE0)
  gl.bindTexture(gl.TEXTURE_2D, targetTexture2);

  {
    // define size and format of level 0
    const level3 = 0;
    const internalFormat3 = gl.RGBA;
    const border3 = 0;
    const format3 = gl.RGBA;
    const type3 = gl.UNSIGNED_BYTE;
    const data3 = null;
    gl.texImage2D(gl.TEXTURE_2D, level3, internalFormat3,
                  targetTextureWidth, targetTextureHeight, border3,
                  format3, type3, data3);

    // set the filtering so we don't need mips
    gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, gl.LINEAR);
    gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_S, gl.CLAMP_TO_EDGE);
    gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_T, gl.CLAMP_TO_EDGE);
  }

  // create to render to
  const targetTexture2_depth = gl.createTexture();
  gl.activeTexture(gl.TEXTURE1)
  gl.bindTexture(gl.TEXTURE_2D, targetTexture2_depth);

  {
    // define size and format of level 0
    const level4 = 0;
    const internalFormat4 = gl.DEPTH_COMPONENT;
    const border4 = 0;
    const format4 = gl.DEPTH_COMPONENT;
    const type4 = gl.UNSIGNED_SHORT;
    const data4 = null;
    gl.texImage2D(gl.TEXTURE_2D, level4, internalFormat4,
                  targetTextureWidth, targetTextureHeight, border4,
                  format4, type4, data4);

    // set the filtering so we don't need mips
    gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, gl.LINEAR);
    gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_S, gl.CLAMP_TO_EDGE);
    gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_T, gl.CLAMP_TO_EDGE);
  }

  // END MAKE TEXTURE
  // START MAKE FRAME BUFFER FOR TEXTURE

  // Create and bind the framebuffer
  const fb = gl.createFramebuffer();
  gl.bindFramebuffer(gl.FRAMEBUFFER, fb);

  // attach the texture as the first color attachment
  gl.framebufferTexture2D(gl.FRAMEBUFFER, gl.COLOR_ATTACHMENT0, gl.TEXTURE_2D, targetTexture, 0);

  // Create and bind another framebuffer
  const fb2 = gl.createFramebuffer();
  gl.bindFramebuffer(gl.FRAMEBUFFER, fb2);

  // attach the texture as the first color attachment
  gl.framebufferTexture2D(gl.FRAMEBUFFER, gl.COLOR_ATTACHMENT0, gl.TEXTURE_2D, targetTexture2, 0);
  // attach the depth texture as the depth attachment
  gl.framebufferTexture2D(gl.FRAMEBUFFER, gl.DEPTH_ATTACHMENT, gl.TEXTURE_2D, targetTexture2_depth, 0);

  var numFrameBuffers = 3;
  var fbs = [];
  var color_textures = [];
  // var depth_textures = [];

  for (var ii = 0; ii < (numFrameBuffers + 1); ii++) {
    // create to render to
    color_textures.push(gl.createTexture());
    gl.activeTexture(gl.TEXTURE0)
    gl.bindTexture(gl.TEXTURE_2D, color_textures[ii]);

    {
      // define size and format of level 0
      const level5 = 0;
      const internalFormat5 = gl.RGBA;
      const border5 = 0;
      const format5 = gl.RGBA;
      const type5 = gl.UNSIGNED_BYTE;
      const data5 = null;
      gl.texImage2D(gl.TEXTURE_2D, level5, internalFormat5,
                    targetTextureWidth, targetTextureHeight, border5,
                    format5, type5, data5);

      // set the filtering so we don't need mips
      gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, gl.LINEAR);
      gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_S, gl.CLAMP_TO_EDGE);
      gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_T, gl.CLAMP_TO_EDGE);
    }

    // Create and bind another framebuffer
    fbs.push(gl.createFramebuffer());
    gl.bindFramebuffer(gl.FRAMEBUFFER, fbs[ii]);

    // attach the texture as the first color attachment
    gl.framebufferTexture2D(gl.FRAMEBUFFER, gl.COLOR_ATTACHMENT0, gl.TEXTURE_2D, color_textures[ii], 0);
  }

  // END MAKE FRAME BUFFER FOR TEXTURE



  // Create a buffer for positions
  var positionBuffer = gl.createBuffer();
  // Bind it to ARRAY_BUFFER (think of it as ARRAY_BUFFER = positionBuffer)
  gl.bindBuffer(gl.ARRAY_BUFFER, positionBuffer);
  // Put the positions in the buffer
  setGeometry(gl);

  // Create a buffer for positions
  var positionBuffer2 = gl.createBuffer();
  // Bind it to ARRAY_BUFFER (think of it as ARRAY_BUFFER = positionBuffer)
  gl.bindBuffer(gl.ARRAY_BUFFER, positionBuffer2);
  // Put the positions in the buffer
  setGeometry2(gl);

  // provide texture coordinates for the rectangle.
  var texcoordBuffer = gl.createBuffer();
  gl.bindBuffer(gl.ARRAY_BUFFER, texcoordBuffer);
  // Set Texcoords.
  setTexcoords(gl);

  // provide texture coordinates for the rectangle.
  var texcoordBuffer2 = gl.createBuffer();
  gl.bindBuffer(gl.ARRAY_BUFFER, texcoordBuffer2);
  // Set Texcoords.
  setTexcoords2(gl, 300 / targetTextureHeight, 400 / targetTextureWidth);

  // Create a texture.
  var texture = gl.createTexture();
  gl.activeTexture(gl.TEXTURE0)
  gl.bindTexture(gl.TEXTURE_2D, texture);
  // Fill the texture with a 4x4 LUMINANCE pixel.
  const level = 0;
  const format = gl.LUMINANCE;
  const type = gl.UNSIGNED_BYTE;
  const border = 0;
  const width = 4;
  const height = 4;
  const pixels = new Uint8Array([
    255, 128, 255, 128,
    128, 255, 128, 255,
    255, 128, 255, 128,
    128, 255, 128, 255,
  ]);
  gl.texImage2D(gl.TEXTURE_2D, level, format, width, height, border,
                format, type, pixels);
  gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MAG_FILTER, gl.NEAREST);
  gl.generateMipmap(gl.TEXTURE_2D);

  function radToDeg(r) {
    return r * 180 / Math.PI;
  }

  function degToRad(d) {
    return d * Math.PI / 180;
  }

  var fieldOfViewRadians = degToRad(60);
  var modelXRotationRadians = degToRad(0);
  var modelYRotationRadians = degToRad(0);

  // Get the starting time.
  var then = 0;
  var i_fbs_current = 0;

  requestAnimationFrame(drawScene);

  // Draw the scene.
  function drawScene(time, aspect) {
    // convert to seconds
    time *= 0.001;
    // Subtract the previous time from the current time
    var deltaTime = time - then;
    // Remember the current time for the next frame.
    then = time;

    // Animate the rotation
    modelYRotationRadians += -0.7 * deltaTime;
    modelXRotationRadians += -0.4 * deltaTime;

    {
      // Tell it to use our program (pair of shaders)
      gl.useProgram(program);

      // render to our targetTexture by binding the framebuffer
      gl.bindFramebuffer(gl.FRAMEBUFFER, fb);

      // render cube with our 3x2 texture
      gl.activeTexture(gl.TEXTURE0)
      gl.bindTexture(gl.TEXTURE_2D, texture);

      // Tell WebGL how to convert from clip space to pixels
      gl.viewport(0, 0, targetTextureWidth, targetTextureHeight);

      gl.clearColor(0.4, 0.4, 0.4, 1)

      // Clear the canvas AND the depth buffer.
      gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);

      // Set aspect ratio
      var aspect = targetTextureWidth / targetTextureHeight;
      // draw cube
      drawCube(aspect, modelYRotationRadians, modelXRotationRadians, time)
    }
    {
      // Tell it to use our program (pair of shaders)
      gl.useProgram(program);

      // render to the canvas
      gl.bindFramebuffer(gl.FRAMEBUFFER, fb2);

      // render cube with our smaller cube texture
      gl.activeTexture(gl.TEXTURE0)
      gl.bindTexture(gl.TEXTURE_2D, targetTexture);

      // resize the canvas
      webglUtils.resizeCanvasToDisplaySize(gl.canvas);

      // Tell WebGL how to convert from clip space to pixels
      gl.viewport(0, 0, gl.canvas.width, gl.canvas.height);

      gl.enable(gl.CULL_FACE);
      gl.enable(gl.DEPTH_TEST);

      gl.clearColor(1, 1, 1, 1)

      // Clear the canvas AND the depth buffer.
      gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);

      // Set aspect ratio
      var aspect = gl.canvas.clientWidth / gl.canvas.clientHeight;
      // draw cube
      drawCube(aspect, modelYRotationRadians, modelXRotationRadians, time)
    }
    {
      // Tell it to use our program (pair of shaders)
      gl.useProgram(program);

      // render to the canvas
      gl.bindFramebuffer(gl.FRAMEBUFFER, fbs[i_fbs_current]);

      // render square with our bigger cube texture
      gl.activeTexture(gl.TEXTURE0)
      gl.bindTexture(gl.TEXTURE_2D, targetTexture2);
      gl.activeTexture(gl.TEXTURE1)
      gl.bindTexture(gl.TEXTURE_2D, targetTexture2_depth);

      // resize the canvas
      webglUtils.resizeCanvasToDisplaySize(gl.canvas);

      // Tell WebGL how to convert from clip space to pixels
      gl.viewport(0, 0, gl.canvas.width, gl.canvas.height);

      gl.enable(gl.CULL_FACE);
      gl.enable(gl.DEPTH_TEST);

      gl.clearColor(0.1, 0.1, 0.1, 1)

      // Clear the canvas AND the depth buffer.
      gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);

      // Tell the shader to use appropriate texture units for texture uniforms
      gl.uniform1i(textureLocation, 0);
      gl.uniform1i(depthTextureLocation, 1);

      var depth = document.getElementById('depth');
      var focal_dist = document.getElementById('focal_dist').value;

      if (depth.checked) {
        var depth_blurring = 1.0;
      } else {
        var depth_blurring = 0.0;
      }

      gl.uniform1f(depthBlurringLocation, depth_blurring);
      gl.uniform1f(focalDistLocation, focal_dist);

      // Set aspect ratio
      var aspect = gl.canvas.clientWidth / gl.canvas.clientHeight;
      // draw square
      drawSquare(aspect, positionLocation, texcoordLocation, matrixLocation);

    }

    if(true) {
      // Tell it to use our program (pair of shaders)
      gl.useProgram(program_post);

      // render to the canvas
      gl.bindFramebuffer(gl.FRAMEBUFFER, null);

      // render square with our bigger cube texture
      gl.activeTexture(gl.TEXTURE0)
      gl.bindTexture(gl.TEXTURE_2D, color_textures[0]);
      gl.activeTexture(gl.TEXTURE1)
      gl.bindTexture(gl.TEXTURE_2D, color_textures[1]);
      gl.activeTexture(gl.TEXTURE2)
      gl.bindTexture(gl.TEXTURE_2D, color_textures[2]);

      // resize the canvas
      webglUtils.resizeCanvasToDisplaySize(gl.canvas);

      // Tell WebGL how to convert from clip space to pixels
      gl.viewport(0, 0, gl.canvas.width, gl.canvas.height);

      gl.enable(gl.CULL_FACE);
      gl.enable(gl.DEPTH_TEST);

      gl.clearColor(0.1, 0.1, 0.1, 1)

      // Clear the canvas AND the depth buffer.
      gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);

      // Tell the shader to use appropriate texture units for tex_*
      gl.uniform1i(tex_curLocation, 0);
      gl.uniform1i(tex_prev_1Location, 1);
      gl.uniform1i(tex_prev_2Location, 2);

      // Set aspect ratio
      var aspect = gl.canvas.clientWidth / gl.canvas.clientHeight;
      // draw square
      drawSquare(aspect, positionLocation_post, texcoordLocation_post, matrixLocation_post);

    }

    i_fbs_current = (i_fbs_current + 1) % numFrameBuffers;
    requestAnimationFrame(drawScene)
  }

  function drawSquare(aspect, pos_attrib, tex_attrib, mat_uniform) {

    // Turn on the position attribute
    gl.enableVertexAttribArray(pos_attrib);

    // Bind the position buffer.
    gl.bindBuffer(gl.ARRAY_BUFFER, positionBuffer2);

    // Tell the position attribute how to get data out of positionBuffer2 (ARRAY_BUFFER)
    var size = 2;          // 2 components per iteration
    var type = gl.FLOAT;   // the data is 32bit floats
    var normalize = false; // don't normalize the data
    var stride = 0;        // 0 = move forward size * sizeof(type) each iteration to get the next position
    var offset = 0;        // start at the beginning of the buffer
    gl.vertexAttribPointer( pos_attrib, size, type, normalize, stride, offset)

    // Turn on the teccord attribute
    gl.enableVertexAttribArray(tex_attrib);

    // Bind the position buffer.
    gl.bindBuffer(gl.ARRAY_BUFFER, texcoordBuffer2);

    // Tell the position attribute how to get data out of texcoordBuffer (ARRAY_BUFFER)
    var size = 2;          // 2 components per iteration
    var type = gl.FLOAT;   // the data is 32bit floats
    var normalize = false; // don't normalize the data
    var stride = 0;        // 0 = move forward size * sizeof(type) each iteration to get the next position
    var offset = 0;        // start at the beginning of the buffer
    gl.vertexAttribPointer( tex_attrib, size, type, normalize, stride, offset)

    // Set the matrix.
    gl.uniformMatrix4fv(mat_uniform, false, m4.identity());

    // Draw the geometry.
    gl.drawArrays(gl.TRIANGLES, 0, 6);

  }

  function drawCube(aspect, modelYRotationRadians, modelXRotationRadians, time) {

    // Turn on the position attribute
    gl.enableVertexAttribArray(positionLocation);

    // Bind the position buffer.
    gl.bindBuffer(gl.ARRAY_BUFFER, positionBuffer);

    // Tell the position attribute how to get data out of positionBuffer (ARRAY_BUFFER)
    var size = 3;          // 3 components per iteration
    var type = gl.FLOAT;   // the data is 32bit floats
    var normalize = false; // don't normalize the data
    var stride = 0;        // 0 = move forward size * sizeof(type) each iteration to get the next position
    var offset = 0;        // start at the beginning of the buffer
    gl.vertexAttribPointer(
        positionLocation, size, type, normalize, stride, offset)

    // Turn on the teccord attribute
    gl.enableVertexAttribArray(texcoordLocation);

    // Bind the position buffer.
    gl.bindBuffer(gl.ARRAY_BUFFER, texcoordBuffer);

    // Tell the position attribute how to get data out of positionBuffer (ARRAY_BUFFER)
    var size = 2;          // 2 components per iteration
    var type = gl.FLOAT;   // the data is 32bit floats
    var normalize = false; // don't normalize the data
    var stride = 0;        // 0 = move forward size * sizeof(type) each iteration to get the next position
    var offset = 0;        // start at the beginning of the buffer
    gl.vertexAttribPointer(
        texcoordLocation, size, type, normalize, stride, offset)

    // Compute the projection matrix
    var projectionMatrix = m4.perspective(fieldOfViewRadians, aspect, 1, 20);

    var speed = document.getElementById("speed").value;
    var scrub = document.getElementById("scrub").value;
    if (scrub == 0) {
      var val = time*speed;
    } else {
      var val = scrub;
    }
    var cameraPosition = [0, 0, 8 + 6*Math.cos(val*0.5)];
    var up = [0, 1, 0];
    var target = [0, 0, 0];

    // Compute the camera's matrix using look at.
    var cameraMatrix = m4.lookAt(cameraPosition, target, up);

    // Make a view matrix from the camera matrix.
    var viewMatrix = m4.inverse(cameraMatrix);

    var viewProjectionMatrix = m4.multiply(projectionMatrix, viewMatrix);

    var matrix = m4.xRotate(viewProjectionMatrix, modelXRotationRadians);
    matrix = m4.yRotate(matrix, modelYRotationRadians);

    // Set the matrix.
    gl.uniformMatrix4fv(matrixLocation, false, matrix);

    // Tell the shader to use texture unit 0 for u_texture
    gl.uniform1i(textureLocation, 0);
    gl.uniform1i(depthTextureLocation, 1);
    gl.uniform1f(depthBlurringLocation, 0);

    // Draw the geometry.
    gl.drawArrays(gl.TRIANGLES, 0, 6 * 6);
  }
}

// Fill the buffer with the values that define a cube.
function setGeometry(gl) {
  var positions = new Float32Array(
    [
    -0.5, -0.5,  -0.5,
    -0.5,  0.5,  -0.5,
     0.5, -0.5,  -0.5,
    -0.5,  0.5,  -0.5,
     0.5,  0.5,  -0.5,
     0.5, -0.5,  -0.5,

    -0.5, -0.5,   0.5,
     0.5, -0.5,   0.5,
    -0.5,  0.5,   0.5,
    -0.5,  0.5,   0.5,
     0.5, -0.5,   0.5,
     0.5,  0.5,   0.5,

    -0.5,   0.5, -0.5,
    -0.5,   0.5,  0.5,
     0.5,   0.5, -0.5,
    -0.5,   0.5,  0.5,
     0.5,   0.5,  0.5,
     0.5,   0.5, -0.5,

    -0.5,  -0.5, -0.5,
     0.5,  -0.5, -0.5,
    -0.5,  -0.5,  0.5,
    -0.5,  -0.5,  0.5,
     0.5,  -0.5, -0.5,
     0.5,  -0.5,  0.5,

    -0.5,  -0.5, -0.5,
    -0.5,  -0.5,  0.5,
    -0.5,   0.5, -0.5,
    -0.5,  -0.5,  0.5,
    -0.5,   0.5,  0.5,
    -0.5,   0.5, -0.5,

     0.5,  -0.5, -0.5,
     0.5,   0.5, -0.5,
     0.5,  -0.5,  0.5,
     0.5,  -0.5,  0.5,
     0.5,   0.5, -0.5,
     0.5,   0.5,  0.5,

    ]);
  gl.bufferData(gl.ARRAY_BUFFER, positions, gl.STATIC_DRAW);
}

// Fill the buffer with the values that define a cube.
function setGeometry2(gl) {
  var positions = new Float32Array(
    [
    -1.0,   1.0,
    -1.0,  -1.0,
    1.0,   -1.0,

    1.0,  -1.0,
    1.0,   1.0,
    -1.0,  1.0,]);

  gl.bufferData(gl.ARRAY_BUFFER, positions, gl.STATIC_DRAW);
}

// Fill the buffer with texture coordinates the cube.
function setTexcoords(gl) {
  gl.bufferData(
      gl.ARRAY_BUFFER,
      new Float32Array(
        [
          0, 0,
          0, 1,
          1, 0,
          0, 1,
          1, 1,
          1, 0,

          0, 0,
          0, 1,
          1, 0,
          1, 0,
          0, 1,
          1, 1,

          0, 0,
          0, 1,
          1, 0,
          0, 1,
          1, 1,
          1, 0,

          0, 0,
          0, 1,
          1, 0,
          1, 0,
          0, 1,
          1, 1,

          0, 0,
          0, 1,
          1, 0,
          0, 1,
          1, 1,
          1, 0,

          0, 0,
          0, 1,
          1, 0,
          1, 0,
          0, 1,
          1, 1,

      ]),
      gl.STATIC_DRAW);
}

// Fill the buffer with texture coordinates the cube.
function setTexcoords2(gl, height, width) {
  gl.bufferData(
      gl.ARRAY_BUFFER,
      new Float32Array(
        [
          0,     height,
          0,     0,
          width, 0,

          width, 0,
          width, height,
          0,     height,
          // 0,     0.14,
          // 0,     0,
          // 0.19, 0,
          //
          // 0.19, 0,
          // 0.19, 0.14,
          // 0,    0.14,
      ]),
      gl.STATIC_DRAW);
}

main();
